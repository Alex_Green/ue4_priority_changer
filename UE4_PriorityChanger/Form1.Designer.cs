﻿namespace UE4_PriorityChanger
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button_StartStop = new System.Windows.Forms.Button();
            this.comboBox_UE4Editor = new System.Windows.Forms.ComboBox();
            this.comboBox_ShaderCompilerWorker = new System.Windows.Forms.ComboBox();
            this.timer_ProcessChecker = new System.Windows.Forms.Timer(this.components);
            this.label_UE4Editor = new System.Windows.Forms.Label();
            this.label_ShaderCompiler = new System.Windows.Forms.Label();
            this.notifyIcon_Tray = new System.Windows.Forms.NotifyIcon(this.components);
            this.label_Discord = new System.Windows.Forms.Label();
            this.comboBox_Discord = new System.Windows.Forms.ComboBox();
            this.comboBox_Skype = new System.Windows.Forms.ComboBox();
            this.label_Skype = new System.Windows.Forms.Label();
            this.label_UnrealCEFSubProcess = new System.Windows.Forms.Label();
            this.comboBox_UnrealCEFSubProcess = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // button_StartStop
            // 
            this.button_StartStop.Location = new System.Drawing.Point(321, 49);
            this.button_StartStop.Name = "button_StartStop";
            this.button_StartStop.Size = new System.Drawing.Size(92, 48);
            this.button_StartStop.TabIndex = 0;
            this.button_StartStop.Text = "Start";
            this.button_StartStop.UseVisualStyleBackColor = true;
            this.button_StartStop.Click += new System.EventHandler(this.button_StartStop_Click);
            // 
            // comboBox_UE4Editor
            // 
            this.comboBox_UE4Editor.FormattingEnabled = true;
            this.comboBox_UE4Editor.Location = new System.Drawing.Point(153, 9);
            this.comboBox_UE4Editor.Name = "comboBox_UE4Editor";
            this.comboBox_UE4Editor.Size = new System.Drawing.Size(162, 21);
            this.comboBox_UE4Editor.TabIndex = 1;
            // 
            // comboBox_ShaderCompilerWorker
            // 
            this.comboBox_ShaderCompilerWorker.FormattingEnabled = true;
            this.comboBox_ShaderCompilerWorker.Location = new System.Drawing.Point(153, 36);
            this.comboBox_ShaderCompilerWorker.Name = "comboBox_ShaderCompilerWorker";
            this.comboBox_ShaderCompilerWorker.Size = new System.Drawing.Size(162, 21);
            this.comboBox_ShaderCompilerWorker.TabIndex = 2;
            // 
            // timer_ProcessChecker
            // 
            this.timer_ProcessChecker.Interval = 50;
            this.timer_ProcessChecker.Tick += new System.EventHandler(this.timer_ProcessChecker_Tick);
            // 
            // label_UE4Editor
            // 
            this.label_UE4Editor.Location = new System.Drawing.Point(15, 9);
            this.label_UE4Editor.Name = "label_UE4Editor";
            this.label_UE4Editor.Size = new System.Drawing.Size(132, 21);
            this.label_UE4Editor.TabIndex = 3;
            this.label_UE4Editor.Text = "Editor Priority:";
            this.label_UE4Editor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_ShaderCompiler
            // 
            this.label_ShaderCompiler.Location = new System.Drawing.Point(12, 36);
            this.label_ShaderCompiler.Name = "label_ShaderCompiler";
            this.label_ShaderCompiler.Size = new System.Drawing.Size(135, 21);
            this.label_ShaderCompiler.TabIndex = 4;
            this.label_ShaderCompiler.Text = "Shader Compile Worker:";
            this.label_ShaderCompiler.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // notifyIcon_Tray
            // 
            this.notifyIcon_Tray.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon_Tray.Icon")));
            this.notifyIcon_Tray.DoubleClick += new System.EventHandler(this.notifyIcon_Tray_DoubleClick);
            // 
            // label_Discord
            // 
            this.label_Discord.Location = new System.Drawing.Point(12, 90);
            this.label_Discord.Name = "label_Discord";
            this.label_Discord.Size = new System.Drawing.Size(135, 21);
            this.label_Discord.TabIndex = 5;
            this.label_Discord.Text = "Discord:";
            this.label_Discord.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBox_Discord
            // 
            this.comboBox_Discord.FormattingEnabled = true;
            this.comboBox_Discord.Location = new System.Drawing.Point(153, 90);
            this.comboBox_Discord.Name = "comboBox_Discord";
            this.comboBox_Discord.Size = new System.Drawing.Size(162, 21);
            this.comboBox_Discord.TabIndex = 6;
            // 
            // comboBox_Skype
            // 
            this.comboBox_Skype.FormattingEnabled = true;
            this.comboBox_Skype.Location = new System.Drawing.Point(153, 117);
            this.comboBox_Skype.Name = "comboBox_Skype";
            this.comboBox_Skype.Size = new System.Drawing.Size(162, 21);
            this.comboBox_Skype.TabIndex = 8;
            // 
            // label_Skype
            // 
            this.label_Skype.Location = new System.Drawing.Point(12, 117);
            this.label_Skype.Name = "label_Skype";
            this.label_Skype.Size = new System.Drawing.Size(135, 21);
            this.label_Skype.TabIndex = 7;
            this.label_Skype.Text = "Skype:";
            this.label_Skype.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_UnrealCEFSubProcess
            // 
            this.label_UnrealCEFSubProcess.Location = new System.Drawing.Point(12, 63);
            this.label_UnrealCEFSubProcess.Name = "label_UnrealCEFSubProcess";
            this.label_UnrealCEFSubProcess.Size = new System.Drawing.Size(135, 21);
            this.label_UnrealCEFSubProcess.TabIndex = 10;
            this.label_UnrealCEFSubProcess.Text = "Unreal CEF Sub Process:";
            this.label_UnrealCEFSubProcess.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBox_UnrealCEFSubProcess
            // 
            this.comboBox_UnrealCEFSubProcess.FormattingEnabled = true;
            this.comboBox_UnrealCEFSubProcess.Location = new System.Drawing.Point(153, 63);
            this.comboBox_UnrealCEFSubProcess.Name = "comboBox_UnrealCEFSubProcess";
            this.comboBox_UnrealCEFSubProcess.Size = new System.Drawing.Size(162, 21);
            this.comboBox_UnrealCEFSubProcess.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 147);
            this.Controls.Add(this.label_UnrealCEFSubProcess);
            this.Controls.Add(this.comboBox_UnrealCEFSubProcess);
            this.Controls.Add(this.comboBox_Skype);
            this.Controls.Add(this.label_Skype);
            this.Controls.Add(this.comboBox_Discord);
            this.Controls.Add(this.label_Discord);
            this.Controls.Add(this.label_ShaderCompiler);
            this.Controls.Add(this.label_UE4Editor);
            this.Controls.Add(this.comboBox_ShaderCompilerWorker);
            this.Controls.Add(this.comboBox_UE4Editor);
            this.Controls.Add(this.button_StartStop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UE4 Priority Changer";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_StartStop;
        private System.Windows.Forms.ComboBox comboBox_UE4Editor;
        private System.Windows.Forms.ComboBox comboBox_ShaderCompilerWorker;
        private System.Windows.Forms.Timer timer_ProcessChecker;
        private System.Windows.Forms.Label label_UE4Editor;
        private System.Windows.Forms.Label label_ShaderCompiler;
        private System.Windows.Forms.NotifyIcon notifyIcon_Tray;
        private System.Windows.Forms.Label label_Discord;
        private System.Windows.Forms.ComboBox comboBox_Discord;
        private System.Windows.Forms.ComboBox comboBox_Skype;
        private System.Windows.Forms.Label label_Skype;
        private System.Windows.Forms.Label label_UnrealCEFSubProcess;
        private System.Windows.Forms.ComboBox comboBox_UnrealCEFSubProcess;
    }
}

