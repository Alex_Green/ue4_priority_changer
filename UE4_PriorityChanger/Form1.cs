﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UE4_PriorityChanger
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.AboveNormal;
            notifyIcon_Tray.Text = Text;

            comboBox_UE4Editor.DataSource = Enum.GetValues(typeof(ProcessPriorityClass));
            comboBox_UE4Editor.SelectedItem = ProcessPriorityClass.BelowNormal;

            comboBox_ShaderCompilerWorker.DataSource = Enum.GetValues(typeof(ProcessPriorityClass));
            comboBox_ShaderCompilerWorker.SelectedItem = ProcessPriorityClass.Idle;

            comboBox_UnrealCEFSubProcess.DataSource = Enum.GetValues(typeof(ProcessPriorityClass));
            comboBox_UnrealCEFSubProcess.SelectedItem = ProcessPriorityClass.Idle;

            comboBox_Discord.DataSource = Enum.GetValues(typeof(ProcessPriorityClass));
            comboBox_Discord.SelectedItem = ProcessPriorityClass.Normal;

            comboBox_Skype.DataSource = Enum.GetValues(typeof(ProcessPriorityClass));
            comboBox_Skype.SelectedItem = ProcessPriorityClass.Normal;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) Close();
        }

        private void button_StartStop_Click(object sender, EventArgs e)
        {
            timer_ProcessChecker.Enabled = !timer_ProcessChecker.Enabled;
            button_StartStop.Text = (timer_ProcessChecker.Enabled ? "Stop" : "Start");
        }

        private void timer_ProcessChecker_Tick(object sender, EventArgs e)
        {
            try
            {
                ProcessPriorityClass UE4Editor_Priority = (ProcessPriorityClass)comboBox_UE4Editor.SelectedItem;
                Process[] Proc_UE4 = Process.GetProcessesByName("UE4Editor");
                foreach (Process p in Proc_UE4)
                {
                    if (!p.HasExited && p.PriorityClass != UE4Editor_Priority)
                        p.PriorityClass = UE4Editor_Priority;
                }

                ProcessPriorityClass UE4SCW_Priority = (ProcessPriorityClass)comboBox_ShaderCompilerWorker.SelectedItem;
                Process[] Proc_SCW = Process.GetProcessesByName("ShaderCompileWorker");
                foreach (Process p in Proc_SCW)
                {
                    if (!p.HasExited && p.PriorityClass != UE4SCW_Priority)
                        p.PriorityClass = UE4SCW_Priority;
                }

                ProcessPriorityClass UE4CEF_Priority = (ProcessPriorityClass)comboBox_UnrealCEFSubProcess.SelectedItem;
                Process[] Proc_CEF = Process.GetProcessesByName("UnrealCEFSubProcess");
                foreach (Process p in Proc_CEF)
                {
                    if (!p.HasExited && p.PriorityClass != UE4CEF_Priority)
                        p.PriorityClass = UE4CEF_Priority;
                }

                ProcessPriorityClass Discord_Priority = (ProcessPriorityClass)comboBox_Discord.SelectedItem;
                Process[] Proc_Discord = Process.GetProcessesByName("Discord");
                foreach (Process p in Proc_Discord)
                {
                    if (!p.HasExited && p.PriorityClass != Discord_Priority)
                        p.PriorityClass = Discord_Priority;
                }

                ProcessPriorityClass Skype_Priority = (ProcessPriorityClass)comboBox_Skype.SelectedItem;
                Process[] Proc_Skype = Process.GetProcessesByName("Skype");
                foreach (Process p in Proc_Skype)
                {
                    if (!p.HasExited && p.PriorityClass != Skype_Priority)
                        p.PriorityClass = Skype_Priority;
                }
            }
            catch { }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                notifyIcon_Tray.Visible = true;
                Hide();
            }
            else if (FormWindowState.Normal == WindowState)
                notifyIcon_Tray.Visible = false;
        }

        private void notifyIcon_Tray_DoubleClick(object sender, EventArgs e)
        {
            notifyIcon_Tray.Visible = false;
            Show();
            WindowState = FormWindowState.Normal;
        }
    }
}
